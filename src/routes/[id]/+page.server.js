export const prerender = 'auto'

/** @type {import('./$types').PageServerLoad} */
export async function load ({ params }) {
  return { title: params.id }
};
